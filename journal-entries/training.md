# Training Neural Networks

* Training neural network is done through a process called backpropagation which I don't understand on a really deep level - these are my new notes.
* Traditional forward propagation works through a simple sigma function that changes the weights of the output to compute the input of the next layer until error is reduced. This is really computationally intensive
* Backpropagation works backwards by changing each  weight in the network after comparing th predicted output with the desired output for that node. To do that you use dE/dw~ij~ which computes how error changes in respect to a weight. Using the error derivatives you can use an update rule to change the weights.
* Tensorflow luckily does the calculus for you. Backpropagation relies on gradients and differentiable functions. Near the input the gradients can become so small that learning no longer happens, however `ReLU` can fix this.
* Be careful doing `ReLU` because if the weighted sum falls below 0 then the `ReLU` will drop everything causing 0 activation.
* With high learning rate it can result in a NaN because of exploding gradients. The weights combined with how the update rule works causes many issues. However, batch normalization can help.
* You should normalize the feature values so that the neural network has to do less work - feature engineering. 
* You can also use dropout regularization which drops out random nodes or units within single gradient steps - a way to reduce complexity.

