# Training and Test Sets

* This expands the ideas of generalization
* If you can only do one sample from your population you want to take a SRS of the draw to split into a training set and a test.
* The balance between the two sets is really difficult with only one draw - the larger the training data the better the model, the larger the test set the larger the confidence intervals.
* Cross-validation can be used to account for only have one draw.
* **Never** train on test data, it will seriously mess up your model.

### Low Amount of Training Data

### ![Screenshot from 2020-01-29 22-28-48](/home/k3sc0re/Pictures/train1.png)

### 50-50 Split

![Screenshot from 2020-01-29 22-30-34](/home/k3sc0re/Pictures/train2.png)