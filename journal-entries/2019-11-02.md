# 2019-12-19

Today I did some more research for my personal project. I was able to meet with Dr. Kuchera at Davidson College and learn more about computational physics - a common application for machine learning. I learned about some higher level machine learning like Generative Adversarial Networking or GAN. It can be used to predict output images and clean up input images. Unsupervised learning can be used to predict Monte Carlo events. The most interesting thing has got to be TensorBNN a custom library that allows for Bayesian calculations using machine learning - a previously unbroached field. 

