# Validation

* As discussed in my previous 2 journal entries, overfit is the bane of any good model and we must avoid it because without over fit we simply have a regression that can't be extrapolated to the rest of my data.
* Using the "test set" and "training set" model is a good idea - but it introduces 1 key problem, you can still overfit - simply just to the 2 datasets you're looking at - especially if you're using a genetic model which does many, many iterations which will very often cause a model that has no larger generalization. This is why we further split our "test set" into a "test set" and "validation set". You train your model on the "training" and "validation" sets and then to reduce exposure you only verify once or twice on the test set.
* This is what my latest iteration of the gradient descent model does - it introduces the concept of validation into the code.

![A horizontal bar divided into three pieces: 70% of which is the training set, 15% the validation set, and 15% the test set](https://developers.google.com/machine-learning/crash-course/images/PartitionThreeSets.svg)

