# Logistic Regression

* Logistic regression is a strategy for models where you create long run probabilities rather than direct categories.
* For example a linear model of coin flips with abnormal coins - you could build a model for angle of bend and coin mass as features.
* This model of regression can often create probabilities outside of 0 -1 which is impossible. You must often create a new model - a logistic model to create probabilities rather than straight categories. This is helpful for spam or not spam email categorization.
* The first step is to put the model into a sigmoid model which will cap probabilities at 0 and 1 - this function's asymptotes. Sigmoid is represented by the function y^'^ = 1/1 + e^-(x)^
* x = b+w~1~ x~1~ + w~2~ x~2~ +... + regularization
* x = log(y/1-y)
* To use a loss model you should use log loss rather than traditional squared loss. The loss gets very high as you approach 0 or 1 to prevent impossible probabilities
* Overfit is a real danger here as these models - so regularization will be key.
* Linear Logistic Regression is very fast and gives very fast prediction times a well. Crossing allows non-linear methodology