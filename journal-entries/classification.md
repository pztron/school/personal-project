# Classification

* I want to convert my model into a classifier using the logistic regression methods that I have learned.
* You can make a threshold so that if a probability is above a certain number it gets classified. .5 isn't always best because it may cause issues with low probability - high severity events.
* Accuracy is one of the best ways to measure a classification of the model. it still has flaws. When certain values are extremely rare - like ad click through -  with accuracy only the model would only predict no click through as it gets the highest accuracy because ad click through is so rare.
* There is a more fine way at looking at the classifier. Through a confusion matrix where there are four categories. This works better for a low chance - high severity events. This allows finer weighing because of the consequences of a false negative.
* This gives us two metric **precision**: How many times were there true positives out of all positive predictions. **recall**: Out of all actual positives, how many were true positives. This creates a tension. Improving recall often means lowering the threshold - but this hurts the precision which requires raising the threshold

![confusion-matrix](/home/k3sc0re/Pictures/confusion-matrix.png)

* If we don't know our threshold beforehand we can do that to. The **ROC Curve** or Receiver Operating Characteristics curve analyses precision and recall for all threshold. The area under the ROC or the **AUC** is the probability that the model will get given two random events a true positive and true negative.
* Prediction Bias is also an issue. We want to insure that our prediction's average is the same as our observation's average/mean. This is an easy canary for something like an incomplete feature set, buggy pipeline, or biased training sample. You should debug the model - not just create a calibration data. You can bin the data to figure out what exactly is going on.
* A calibration plot is an excellent tool as it gives the mean prediction and observation for a given bucket or bin of values. 

