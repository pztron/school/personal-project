# Feature Crosses

* This is the art of creating synthetic features, something I've been doing for a while, but really need to fully understand.
* They are really helpful for fitting non-linear methods to gradient descent
* They may be very sparse if you cross one-hot features which could be bad for your model - I read a paper about this
* I'm going to implement them into my housing model