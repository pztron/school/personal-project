# Representation

- When data is given in the real world it is rarely actually sorted or formated properly

- Creating features or explanatory variables is called feature engineering

- Things that are numerical like a number of rooms are easy

- The hard thing is a categorical variable like a string or text, like the street the house is on, there are several stratagems here

- - One-Hot Encoding: If you only care about one value, give that value a value of 1 and everything else 0

- A feature should occur pretty often

- Features should be clear and obvious, makes optimization much easier

- No magic values, like putting a -1 for houses that have never been on the market, this will mess up models

- The definition of a feature shouldn't change over time, the data should always be in the same units

- Data should not have outliers since much of this is simply regression+, an outlier will hurt the model

- You can bin data - separate data into certain categories and then use those separated bins to make predictions. For instance, you could bin the entire US by city and then see if the east side of cities is more wealthy than the west side.

- Visualize the data before you use it. Plot histograms and the like

- Debug the data to view any duplicates, outliers, and the like

- Look at the descriptive statistics of the data over time - especially if you're adding data.

- I'm now going to implement the concepts into the model which has now been semi-generalized.