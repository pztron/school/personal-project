# Regularization for Sparsity

* I want look more at feature crosses. Since I've converted my algorithm to classification it makes sense to look at regularization and feature crosses again. Especially feature crosses when both features are sparse.
* This causes two issues: RAM Usage (the model becomes massive) and a lot of "Noise" in the model causing overfitting. We need to regularize in a way to zero out weights and reduce model size.
* We should penalize the the number of weights by penalizing the sum of the absolute value of weights.