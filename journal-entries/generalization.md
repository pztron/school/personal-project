# Generalization - 2019-11-02

* Machine learning separates itself from statistics because data can actually be generalized, without generalization we would just be doing regression models
* Some people incorrectly try to make models better by making really complex lines with the training data - this is known as **overfitting**  
* The issue is if new examples are added and the model won't be able to sort them.
* To improve generalization one process is used, the Ockham of **Ockham's Razor** developed generalization theory
* Theory is fine - but how do we do anything - it's pretty simple again, just train on part of the labeled data then practice on labeled data and you know how good your model is given any random dataset. This relies on three premises:
  * Draw examples randomly
  * The **population** is stable
  * Always pull from the same data.

![Screenshot from 2020-01-29 22-13-12](/home/k3sc0re/Pictures/general.png)