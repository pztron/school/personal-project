# Reviews via Embeddings

* If you want to make movie recommendations based on what movies users have watched you need to find the similarity.
* An embedding in 1d would be sorting the movies by whether they're animated or not. Only using 1d could be disastrous as you could recommend a movie like Sausage Party to kids simply because it's animated. You could try adding a 2nd dimension. You can continue adding dimensions until the model is good. The network will just give a Cartesian coordinate to each movie to allow dimensions to be understood by the model. All of this assumes that your features actually have any predictive power. 
* An embedding can actually be really similar because the embedding will just be a hidden layer with one node per dimension - supervised information like users watching the same two movies allows weights to be added to the nodes. 
* Since the matrix will be very sparse you only want to note the movies that have been watched. You just map a slice or dictionary to the movie and then you can associate a list which each user without having the note all of the movies the user did not watch.

![](https://developers.google.com/machine-learning/crash-course/images/InputRepresentationWithValues.png)

* You can even do this for the housing problem. You could create an embedding on the words within a real estate ad to understand how expensive or cheap houses group. You can add the rest of the features as normal inputs.
* I could even do this for the hand written digit and do an embedding on what area on digit is colored in - the embedding much like the multi-class neural network can give a probability distribution just based on the similarities.
* For the movie recommendation you use the movies that were watched as labels sorted through the embedding, and then create an embedding and maybe a multi-class neural network for the other movies.

![](https://developers.google.com/machine-learning/crash-course/images/EmbeddingExample3-1.svg)

* You can even weight each dimension in an embedding to increase or decrease the power of each dimension.
* More dimensions can be dangerous because it can create over fit and slower training. I will generally optimize my number of dimensions to be equal to the fourth root of the possible values.
* I find embeddings really cool because it creates a lot of structure in the generally unstructured supervised learning that I have been doing up to now.

![), verb tense (walking/walked and swimming/swam), and capital cities (Turkey/Ankara and Vietnam/Hanoi)](https://developers.google.com/machine-learning/crash-course/images/linear-relationships.svg)