# Handwritten Digit Multi-Class Neural Network

* I need to build a classification model for one of my 2 "cumulative applications" that allows me to classify multiple things and for that you must use a multi-class neural network - linear models only work for binary-class problems.
* The paradigm here is a one vs all classification. You want to create a unique output node for every class and then use a DNN or separate models to train each class in one vs all. In a DNN you can share all of the internals. You should use softmax that requires the output all one vs all to sum to 1 to make sure there is proper single-label multi-class classification as logistic regression is happening which gives probabilities. Examples may only be a member of one class and using softmax insures this. You need to encode this into the loss.

![](https://developers.google.com/machine-learning/crash-course/images/OneVsAll.svg)

* For multi-class multi-label classification there needs to be no constraints on class membership - unlike the previous paradigm. There are two ways to achieve this:
* Full SoftMax: Just brute forces all of the output nodes and trains them one by one.
* Candidate Sampling: If the output node is very sparse you can take a SRS of the negatives and only train on them. For instance if you were identifying dogs but had just general photos being read into the model you just need to take a SRS of the others, so you may just train on the dogs and then a toaster, calculator, and human.

