# Regularization

* Regularization is the science penalizing the complexity of a model to reduce overfitting as creating super specific synthetic features can be dangerous for a model.
* Often as training loss goes down, validation loss goes up due to over fitting
* One strategy is just to stop training your model to prevent over fit
* The other main paradigm is penalizing complexity in the model
* You can prefer smaller weights
* The main way to penalize weight inflation is through L<sub>2</sub> or ridge regularization

## How Does It Work

* It takes the sum of the squares of the weights
* It uses a Bayesian prior - like a density graph (centered on 0, normal distribution)
* Balance reducing loss vs generalization through λ which controls the ratio of the two.
* With lots of training data you should not use too much regularization
* With little or novel training data use as many regularization methods as needed

