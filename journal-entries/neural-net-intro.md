# Introduction to Neural Networks

* For the entirety of the this project I have only done linear/ gradient descent work - however much of machine learning - especially that which isn't as simple as a prediction needs a more complex model or a neural network.

![](https://developers.google.com/machine-learning/crash-course/images/NonLinearSpiral.png)

* The above image is an excellent example of when we must begin using neural networks - trying to use simple feature crossing and other "tricks" to get non-linear data to fit to a linear model will no longer work. I want a way for the model to figure out the non-linear data without having to sit around and figure out which advanced functions fit to the data - which would kill generalization.
* Neural networks are made to deal with this in particular. They are really good at data that is more soft, and has many, many, many features like music, writing, photos - essentially anything a human could create and sort.
* A linear model is pretty simple - a set amount of inputs weighted linearly and maybe run through a function like sigmoid to produce an output. The hidden layer represents any clever math that had to be done to the non-linear data. We can keep on adding hidden layers - but  the model will stay linear.

![](https://developers.google.com/machine-learning/crash-course/images/linear_net.svg)

![](https://developers.google.com/machine-learning/crash-course/images/1hidden.svg)

* The best way to fix is to break the linear pattern somewhere along the model through an activation functions. There a bunch of ways to convert from linear to non-linear like `RELU` or `Sigmoid` or `tanh`. You train neural networks through a method called backpropagation. Where gradient descent simply used derivatives to move along a curve of the weights vs loss, backpropagation uses the loss function and finds the gradient between each layer with respect to the layer's weights, this process proceeds backwards through the model allowing partial calculations to be used for computation in each layer.

![](https://developers.google.com/machine-learning/crash-course/images/activation.svg)

* I am now going to convert to housing model into a neural network