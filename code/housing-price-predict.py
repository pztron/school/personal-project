"""
Pretty simple program to get used to pandas and learning rates, it uses the number of bedrooms to predict housing price in California
"""

from __future__ import print_function

import math

from IPython import display
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.1f}'.format

# Get the data
california_housing_dataframe = pd.read_csv("california_housing_train.csv", sep=",")



# Randomize the data as to give Stochastic Gradient Descent the best chance - the data might have bias in its order
california_housing_dataframe = california_housing_dataframe.reindex(
    np.random.permutation(california_housing_dataframe.index))
# It's easier to work in units of thousands
california_housing_dataframe["median_house_value"] /= 1000.0
california_housing_dataframe

# Describe the data
print(california_housing_dataframe.describe())

def input_function(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    """Trains a linear regression model of one feature.
  
    Arguments:
      features: pandas DataFrame of features - explanatory variable
      targets: pandas DataFrame of targets - response variable
      batch_size: Size of batches to be passed to the model
      shuffle: True or False. Whether to shuffle the data. - insures that the model won't just make predictions based on data order
      num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
    Returns:
      Tuple of (features, labels) for next data batch
    """
  
    # Convert pandas data into a dictionary of numpy arrays for the features
    features = {key:np.array(value) for key,value in dict(features).items()}                                           
 
    # Construct a dataset, and configure batching/repeating.
    ds = Dataset.from_tensor_slices((features,targets)) # warning: 2GB limit
    ds = ds.batch(batch_size).repeat(num_epochs)
    
    # Shuffle the data, if specified.
    if shuffle:
      ds = ds.shuffle(buffer_size=10000)
    
    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

def train_model(learning_rate, steps, batch_size, input_feature="total_rooms"):
  """Trains a linear regression model of one feature.
  
  Arguments:
    learning_rate: A floating point for the learning rate
    steps: An int for the total number of training steps which cconsists of a forward and 
        backward pass using a single batch.
    batch_size: A non-zero int of the batch size
    input_feature: A string that specifies label the input or explanatory variable
  """
  
  # Do this in 10 steps so we can observe at tenths of the way in
  periods = 10
  steps_per_period = steps / periods

  explanatory = input_feature
  explanatory_data = california_housing_dataframe[[explanatory]]
  response_label = "median_house_value"
  response = california_housing_dataframe[response_label]

  # Create columns for the explanatory variable
  explanatory_columns = [tf.feature_column.numeric_column(explanatory)]
  
  # Create input functions.
  training_input_fn = lambda:input_function(explanatory_data, response, batch_size=batch_size)
  prediction_input_fn = lambda: input_function(explanatory_data, response, num_epochs=1, shuffle=False)
  
  # Create a linear regressor object.
  the_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
  # prevent from running amok
  the_optimizer = tf.contrib.estimator.clip_gradients_by_norm(the_optimizer, 5.0)
  # Set up the regressor
  linear_regressor = tf.estimator.LinearRegressor(
      feature_columns=explanatory_columns,
      optimizer=the_optimizer
  )

  # Set up to plot the state of our model's line each period.
  plt.figure(figsize=(15, 6))
  plt.subplot(1, 2, 1)
  plt.title("Learned Line by Period")
  plt.ylabel(response_label)
  plt.xlabel(explanatory)
  sample = california_housing_dataframe.sample(n=300)
  plt.scatter(sample[explanatory], sample[response_label])
  colors = [cm.RdBu(x) for x in np.linspace(-1, 1, periods)]

  # Train the model, but do so inside such that things can be assesed later
  # loss metrics.
  print("Training model...")
  print("Root Mean Squared Error (on training data):")
  root_mean_squared_errors = []
  for period in range (0, periods):
    # Train the model, starting from the prior state.
    linear_regressor.train(
        input_fn=training_input_fn,
        steps=steps_per_period
    )
    # Get predictions
    predictions = linear_regressor.predict(input_fn=prediction_input_fn)
    predictions = np.array([item['predictions'][0] for item in predictions])
    
    # Get residuals
    root_mean_squared_error = math.sqrt(
        metrics.mean_squared_error(predictions, response))
    # Print the current residual
    print("  period %02d : %0.2f" % (period, root_mean_squared_error))
    # Add the residuals to the list
    root_mean_squared_errors.append(root_mean_squared_error)
    # Track the weights and biases over time.
    # Apply some math to ensure that the data and line are plotted neatly.
    y_extents = np.array([0, sample[response_label].max()])
    
    weight = linear_regressor.get_variable_value('linear/linear_model/%s/weights' % input_feature)[0]
    bias = linear_regressor.get_variable_value('linear/linear_model/bias_weights')

    x_extents = (y_extents - bias) / weight
    x_extents = np.maximum(np.minimum(x_extents,
                                      sample[explanatory].max()),
                           sample[explanatory].min())
    y_extents = weight * x_extents + bias
    plt.plot(x_extents, y_extents, color=colors[period]) 
  print("Model training finished.")

  # Output a graph of loss metrics over periods.
  plt.subplot(1, 2, 2)
  plt.ylabel('Root Mean Squared Error')
  plt.xlabel('Periods')
  plt.title("Root Mean Squared Error vs. Periods")
  plt.tight_layout()
  plt.plot(root_mean_squared_errors)

  # Output a table with calibration data.
  calibration_data = pd.DataFrame()
  calibration_data["predictions"] = pd.Series(predictions)
  calibration_data["targets"] = pd.Series(response)
  display.display(calibration_data.describe())

  print("Final Root Mean Squared Error (on training data): %0.2f" % root_mean_squared_error)
  plt.show()

train_model(
    learning_rate=0.00002,
    steps=500,
    batch_size=5
)



